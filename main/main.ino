#define X 6
#define Y 7
#define analog A0
float alpha = 0.2;
float beta = 0.8;


void setup() {
  Serial.begin(9600);
  pinMode(analog, INPUT);
  charge();
  while(analogRead(analog) < (beta + 0.1)*1024);
  Serial.print("INITIAL CHARGE TO: ");
  Serial.println(toc());
}

void loop() {
  unsigned long measTime = 0;
  discharge();
  while(analogRead(analog) > alpha * 1024);
  measTime = toc();
  Serial.print("DISCHARGE ");
  Serial.println(measTime);
  charge();
  while(analogRead(analog) < beta * 1024);
  measTime = toc();
  Serial.print("CHARGE ");
  Serial.println(measTime);
  
}

unsigned long toc(){
  /*
   * returns time passed from last use of this function
   * for the first time returns 
   * time from turning the circuit on
   */
  static unsigned long last;
  unsigned long t = millis();
  unsigned long toReturn =  t - last;
  last = t;
  return toReturn;
}

void charge(){
  pinMode(X, OUTPUT);
  digitalWrite(X, HIGH);
  pinMode(Y, INPUT);
}

void discharge(){
  pinMode(Y, OUTPUT);
  digitalWrite(Y, LOW);
  pinMode(X, INPUT);
}



