# -*- coding: utf-8 -*-

import serial
import matplotlib.pyplot as plt

def calcC(time, res):
    return time/(1.39 * res)*1000

port = "/dev/ttyACM0"
sio = serial.Serial(port, 9600)
sio.close()
sio.open()
sio.readline()

daneC = [-100]*100
daneD = daneC[:]
daneS = daneD[:]

plt.ion()
fig = plt.figure()
liniaC, = plt.plot(daneC, label='$C_{c}$')
liniaD, = plt.plot(daneD, label='$C_{d}$')
liniaS, = plt.plot(daneD, label='$C_{s}$')
plt.ylim([0, 1000])

try:
    while True:
        try:
            temp = sio.readline().decode('ascii')
            if(temp.split(' ')[0] == "CHARGE"):
                print(temp)
                daneC = daneC[1:]
                daneC.append(calcC(float(temp.split(' ')[1]), 220))
                liniaC.set_ydata(daneC)
            if(temp.split(' ')[0] == "DISCHARGE"):
                print(temp)
                daneD = daneD[1:]
                daneD.append(calcC(float(temp.split(' ')[1]), 10000))
                liniaD.set_ydata(daneD)
            daneS = daneS[1:]
            daneS.append(1/2*(daneC[-1]+daneD[-1]))
            liniaS.set_ydata(daneS)
            plt.legend()
            plt.draw()
            print(daneS[-1])
            plt.pause(0.001)
        except ValueError:
            pass
except KeyboardInterrupt:
    pass
finally:
    sio.close()
